# k8s-ci-helper

Helper image for CI of Kubernetes manifests

## Inclusions

1. [kubeeval](https://github.com/instrumenta/kubeval) - [0.15.0](https://github.com/instrumenta/kubeval/releases/tag/0.15.0)
1. [kube-score](https://github.com/zegl/kube-score) - [1.10.1](https://github.com/zegl/kube-score/releases/tag/v1.10.1)
1. [kubeconform](https://github.com/yannh/kubeconform) - [v0.4.5](https://github.com/yannh/kubeconform/releases/tag/v0.4.4)

## Usage

This can be used in your CI pipeline as follows (using GitLab as an example):

```yaml
stages:
  - test

kubeconform:
  stage: test
  image: registry.gitlab.com/andrewheberle/k8s-ci-helper:latest
  script:
  - kubeconform -verbose -summary -strict -ignore-filename-pattern "\.gitlab-ci\.yml" manifests/
```
