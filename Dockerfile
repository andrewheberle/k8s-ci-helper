FROM alpine:3.13 AS downloader

ENV KUBE_SCORE_VERSION=1.10.1 \
    KUBE_SCORE_BASE="https://github.com/zegl/kube-score" \
    KUBEVAL_VERSION=0.15.0 \
    KUBEVAL_BASE="https://github.com/instrumenta/kubeval" \
    KUBECONFORM_VERSION=0.4.5 \
    KUBECONFORM_BASE="https://github.com/yannh/kubeconform"

# Download and verify kubeval
RUN apk --no-cache add curl && \
    curl -L -O "${KUBEVAL_BASE}/releases/download/${KUBEVAL_VERSION}/kubeval-linux-amd64.tar.gz" && \
    curl -L "${KUBEVAL_BASE}/releases/download/${KUBEVAL_VERSION}/checksums.txt" | \
        grep kubeval-linux-amd64.tar.gz > checksums.txt && \
    sha256sum -c checksums.txt && \
    tar x -z -v -C /usr/local/bin -f kubeval-linux-amd64.tar.gz && \
    chmod +x /usr/local/bin/kubeval

# Download and verify kube-score
RUN curl -L -O "${KUBE_SCORE_BASE}/releases/download/v${KUBE_SCORE_VERSION}/kube-score_${KUBE_SCORE_VERSION}_linux_amd64.tar.gz" && \
    curl -L "${KUBE_SCORE_BASE}/releases/download/v${KUBE_SCORE_VERSION}/checksums.txt" | \
        grep "kube-score_${KUBE_SCORE_VERSION}_linux_amd64.tar.gz" | tee checksums.txt && \
    sha256sum -c checksums.txt && \
    tar x -z -v -C /usr/local/bin -f "kube-score_${KUBE_SCORE_VERSION}_linux_amd64.tar.gz" && \
    chmod +x /usr/local/bin/kube-score

# Download and verify kubconform   
RUN curl -L -O "${KUBECONFORM_BASE}/releases/download/v${KUBECONFORM_VERSION}/kubeconform-linux-amd64.tar.gz" && \
    curl -L "${KUBECONFORM_BASE}/releases/download/v${KUBECONFORM_VERSION}/CHECKSUMS" | \
        grep "kubeconform-linux-amd64.tar.gz" | tee checksums.txt && \
    sha256sum -c checksums.txt && \
    tar x -z -v -C /usr/local/bin -f "kubeconform-linux-amd64.tar.gz" && \
    chmod +x /usr/local/bin/kubeconform

FROM alpine:3.13

COPY --from=downloader \
    /usr/local/bin/kubeval \
    /usr/local/bin/kube-score \
    /usr/local/bin/kubeconform \
    /usr/local/bin/
